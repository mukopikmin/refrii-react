import React, { Component } from 'react';
import {
  Modal, ModalHeader, ModalBody, ModalFooter, Col, Button, Form, FormGroup, Label, Input,
} from 'reactstrap';
import { PropTypes } from 'prop-types';
import 'react-datepicker/dist/react-datepicker.css';

class EditUnitModal extends Component {
  componentDidMount() {
    this.onLabelChange = this.onLabelChange.bind(this);
    this.onStepChange = this.onStepChange.bind(this);
    this.create = this.create.bind(this);
    this.update = this.update.bind(this);
    this.close = this.close.bind(this);
  }

  onLabelChange(e) {
    const { updateParams, params } = this.props;

    updateParams({
      ...params,
      label: e.target.value,
    });
  }

  onStepChange(e) {
    const { updateParams, params } = this.props;

    updateParams({
      ...params,
      step: e.target.value,
    });
  }

  create() {
    const { create, params } = this.props;

    create({
      label: params.label,
      step: params.step,
    });
  }

  update() {
    const { update, params } = this.props;

    update(params);
  }

  close() {
    const { close } = this.props;

    close();
  }

  remove(params) {
    const { remove } = this.props;

    remove(params);
  }

  render() {
    const {
      isEditUnitModalOpen, isNewUnitModalOpen, params,
    } = this.props;
    const isOpen = isNewUnitModalOpen || isEditUnitModalOpen;

    return (
      <Modal isOpen={isOpen} toggle={this.close}>
        <ModalHeader toggle={this.close}>
          {(() => (isEditUnitModalOpen ? 'ラベルの編集' : 'ラベルの追加'))()}
        </ModalHeader>
        <ModalBody>
          <Form>
            <FormGroup row>
              <Label for="label" sm={3}>ラベル</Label>
              <Col sm={9}>
                <Input type="text" name="label" id="label" onChange={this.onLabelChange} value={params.label} />
              </Col>
            </FormGroup>
            <FormGroup row>
              <Label for="step" sm={3}>増減値</Label>
              <Col sm={9}>
                <Input type="number" name="step" id="step" onChange={this.onStepChange} value={params.step} />
              </Col>
            </FormGroup>
          </Form>
        </ModalBody>
        <ModalFooter>
          <Button outline color="secondary" onClick={this.close}>キャンセル</Button>
          {(() => {
            if (isEditUnitModalOpen) {
              return (
                <div>
                  <Button outline color="primary" onClick={this.update}>更新</Button>
                  &nbsp;
                  <Button outline color="danger" onClick={() => this.remove(params)}>削除</Button>
                </div>
              );
            }
            return (
              <Button outline color="primary" onClick={this.create}>追加</Button>
            );
          })()}
        </ModalFooter>
      </Modal>
    );
  }
}

EditUnitModal.propTypes = {
  updateParams: PropTypes.func.isRequired,
  params: PropTypes.shape({
    label: PropTypes.string,
    step: PropTypes.number,
  }),
  create: PropTypes.func.isRequired,
  update: PropTypes.func.isRequired,
  close: PropTypes.func.isRequired,
  remove: PropTypes.func.isRequired,
  isEditUnitModalOpen: PropTypes.bool.isRequired,
  isNewUnitModalOpen: PropTypes.bool.isRequired,
};

EditUnitModal.defaultProps = {
  params: {
    label: '',
    step: 0,
  },
};

export default EditUnitModal;
